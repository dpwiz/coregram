module Coregram.Source
  ( snapshot
  ) where

import           Control.Concurrent (threadDelay)

import qualified Coregram.Source.ProcStats as ProcStats

snapshot :: Int -> IO [Float]
snapshot period = do
  start <- ProcStats.fetch
  threadDelay period
  end <- ProcStats.fetch

  pure $ ProcStats.cpuStats start end
