module Main where

import           Control.Monad (forever)
import           Data.Text (Text)
import qualified Data.Text.IO as Text

import Coregram.Plot (leftBars, joinWith)
import Coregram.Source (snapshot)

main :: IO ()
main = forever $ do
  row <- snapshot 1000000
  Text.putStrLn $ plot row

plot :: (Fractional percent, Ord percent) => [percent] -> Text
plot = joinWith " " . map leftBars
