{-# LANGUAGE Rank2Types #-}

module Coregram.Plot
  ( Plotter
  , leftBars
  , Joiner
  , joinWith
  ) where

import           Data.Text (Text)
import qualified Data.Text as Text

type Plotter f = (Fractional f, Ord f) => f -> Text
type Joiner = [Text] -> Text

leftBars :: Plotter f
leftBars f
  | f < 1/8 = "▏"
  | f < 2/8 = "▎"
  | f < 3/8 = "▍"
  | f < 4/8 = "▌"
  | f < 5/8 = "▋"
  | f < 6/8 = "▊"
  | f < 7/8 = "▉"
  | otherwise  = "█"
  -- assert: 0.0 <= f <= 1.0

joinWith :: Text -> Joiner
joinWith = Text.intercalate
