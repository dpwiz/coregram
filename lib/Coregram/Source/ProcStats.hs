module Coregram.Source.ProcStats
  ( fetch
  , cpuStats
  , Stats
  , Counter(..)
  , Core(..)
  , CPUStats(..)
  , cpuLoad
  , coreUtilization
  , cpuAggrStats
  , cpuCoreStats
  ) where

import           Data.Either (rights)
import           Data.Maybe (listToMaybe)
import           Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import           Data.Text.Read (decimal)
import           Numeric.Natural (Natural)

type Stats = [Counter]

data Counter
  = CPU Core CPUStats
  | Interrupts Int [Int]
  | Ctxt Int
  | BootTime Int
  | Processes Int
  | ProcsRunning Int
  | ProcsBlocked Int
  | SoftIRQ Int [Int]
  | Unknown Text [Int]
  deriving (Show)

data Core = Aggregate | Core Natural
  deriving (Eq, Ord, Show)

data CPUStats = CPUStats
  { user      :: Int
  , nice      :: Int
  , system    :: Int
  , idle      :: Int
  , iowait    :: Int
  , irq       :: Int
  , softirq   :: Int
  , steal     :: Int
  , guest     :: Int
  , guestNice :: Int
  } deriving (Show)

fetch :: IO Stats
fetch = fmap extract (Text.readFile "/proc/stat")
  where
    extract :: Text -> Stats
    extract content = rights
      [ mkCounter metric (asInts columns)
      | metric : columns <- map Text.words (Text.lines content)
      ]

    mkCounter :: Text -> [Int] -> Either Text Counter
    mkCounter metric columns = case metric of
      (Text.splitAt 3 -> ("cpu", n)) ->
        parseCPU n columns
      "intr" ->
        parseIntr columns
      "ctxt" ->
        parseSingle Ctxt "ctxt" columns
      "btime" ->
        parseSingle BootTime "btime" columns
      "processes" ->
        parseSingle Processes "processes" columns
      "procs_running" ->
        parseSingle ProcsRunning "procs_running" columns
      "procs_blocked" ->
        parseSingle ProcsBlocked "procs_blocked" columns
      "softirq" ->
        parseSoftIRQ columns
      _ ->
        Right $ Unknown metric columns

    parseCPU :: Text -> [Int] -> Either Text Counter
    parseCPU (asCore -> Right core) cols = case cols of
      user : nice : system : idle : iowait : irq : softirq : steal : guest : guestNice : _ ->
        Right $ CPU core CPUStats{..}
      _ ->
        Left "Bad cpu stat format"
    parseCPU _ _ =
        Left "Bad cpu stat format"

    parseIntr :: [Int] -> Either Text Counter
    parseIntr cols = case Prelude.splitAt 1 cols of
      ([total], byIntr) ->
        Right $ Interrupts total byIntr
      _ ->
        Left "Bad intr stat format"

    parseSingle :: (Int -> Counter) -> Text -> [Int] -> Either Text Counter
    parseSingle cons metric = \case
      [counter] ->
        Right $ cons counter
      _ ->
        Left $ mconcat ["Bad ", metric, "stat format"]

    parseSoftIRQ :: [Int] -> Either Text Counter
    parseSoftIRQ cols = case Prelude.splitAt 1 cols of
      ([total], byIRQ) ->
        Right $ SoftIRQ total byIRQ
      _ ->
        Left "Bad softirq stat format"

    asInts :: [Text] -> [Int]
    asInts cols =
      [ int
      | Right (int, "") <- map decimal cols
      ]
      -- assert: len cols == len result

    asCore :: Text -> Either Text Core
    asCore = \case
      "" ->
        Right Aggregate
      (decimal -> Right (coreN, ""))
        | coreN >= 0 ->
          Right $ Core coreN
      xz ->
        Left xz

cpuStats :: Stats -> Stats -> [Float]
cpuStats start end = (aggr : byCore)
  where
    Just aggr = cpuLoad
        <$> cpuAggrStats start
        <*> cpuAggrStats end

    byCore =
        [ cpuLoad statsStart statsEnd
        | ((_, statsStart), (_, statsEnd)) <- zip (cpuCoreStats start) (cpuCoreStats end)
        ]

cpuLoad :: CPUStats -> CPUStats -> Float
cpuLoad start end =
  1 - fromIntegral (idleEnd - idleStart) / fromIntegral (totalEnd - totalStart)
  where
    (idleStart, totalStart) = coreUtilization start
    (idleEnd, totalEnd) = coreUtilization end

coreUtilization :: CPUStats -> (Int, Int)
coreUtilization CPUStats{..} = (idle, total)
  where
    total = sum
      [user, nice, system, idle, iowait, irq, softirq, steal, guest, guestNice]

cpuAggrStats :: Stats -> Maybe CPUStats
cpuAggrStats stats = listToMaybe
  [ s
  | CPU Aggregate s <- stats
  ]
  -- assert: no more than one cpu/aggregate counter present

cpuCoreStats :: Stats -> [(Natural, CPUStats)]
cpuCoreStats stats =
  [ (n, s)
  | CPU (Core n) s <- stats
  ]
